#/bin/bash
# --------------------------------------- 
# File Name : job_status1.sh
# Creation Date : 13-05-2015
# Last Modified : Wed May 27 18:55:10 2015
# Created By : wdd 
# --------------------------------------- 
user=${1-wdd}
me=wdd
echo "qstat -u ${user}" | ssh ${me}@login.clsp.jhu.edu 'bash -s' 2> e.tmp | grep @ > tmp
echo "echo \"*********************************Grid Staus************************************\"" > checker.tmp
cat tmp | perl -n -e '@l=split; /.*@(.*)\.clsp.*/; print "echo -n jobid=$l[0],machine=$1,\$(qstat -j $l[0] | grep \"usage\\|resource_list\"), | sed '\''s/ //g'\'' | sed '\''s/hardresource_list://g'\'' | sed '\''s/usage1:/,/g'\'' | perl -n -e '\''/jobid=(.*),machine=(.*?),/;print \"jid=\$1, machine=\$2, \";if(/.*mem_free=(.*?),/){print \"mem_free=\$1, \";}else{print \"mem_free=1G, \";};if(/.*ram_free=(.*?),/){print \"ram_free=\$1, \";}else{print \"ram_free=1G, \";};/vmem=(.*),maxvmem=(.*),/;print \"vmem=\$1, mx_vmem=\$2\\n\"'\''\n"' >> checker.tmp
echo "echo \"\"" >> checker.tmp
echo "echo \"**************************Real Time Staus****************************\"" >> checker.tmp
echo "echo -e \"PID   USER      PR  NI VIRT  RES   SHR S  %CPU %MEM TIME+     COMMAND\"" >> checker.tmp
cat tmp | perl -n -e '/.*@(.*)\.clsp.*/; print "$1\n"' | sort | uniq | perl -n -e '/(\S+)/; print "echo \"---------------------------machine: $1------------------------------\"\necho \"top -n1 -b -u USER | grep \\\"USER\\\" | grep -v \\\"top\\\\\|bash\\\\\|sshd\\\\\|grep\\\"\" | ssh ME\@$1\.clsp\.jhu\.edu '\''bash -s'\''\n"' | sed "s/ME/${me}/g" | sed "s/USER/${user}/g">> checker.tmp;
echo "echo \"*********************************************************************\"" >> checker.tmp
cat checker.tmp | ssh ${me}@login.clsp.jhu.edu 'bash -s' 2> e.tmp
rm tmp checker.tmp e.tmp
