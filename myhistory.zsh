function _useHistory() {
    \egrep -v '^top$|^pwd$|^ls$|^ll$|^l$|^lt$|^cd |^h |^bg$|^fg$|^h$'
}

function _owner() {
    \ls -ld "${1:-.}" | \awk '{print $3}'
}

function _lastHistoryLine() {
    history | tail -n 1 | HISTTIMEFORMAT= \sed 's:^ *[0-9]* *::'
}

function _localHistory() {
    if [ `_owner` = ${USER:=$(whoami)} ] ; then
        local line
        line=$(_lastHistoryLine)
        local cond 
        cond=$(echo "${line}" | _useHistory)
        if [ $cond ] ; then
            echo $(date +'%FT%T').${HOST:=$(uname -n)} "$line" >> .history 2>> /dev/null
        fi
    fi
}

function h() {
    # look for something in your cwd's .history file
    if [[ -r .history ]]; then
        if ! [ $1 ]; then
            \cat .history
        else
            # permit looking for multiple things: h 'foo bar' baz
            local f
            for f in "$@"; do
                grep -a "$f" .history # allow aliased grep --color etc
            done
        fi
    else
        echo "Warning: .history not accessible" 1>&2
    fi
}

precmd() { _localHistory }
